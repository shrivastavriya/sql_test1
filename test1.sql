name : Riya Sanjay Shrivastav
batch(2).intern
assignment : sql test 1



question 1 :  select employees in descending order - salary 
sql query  : SELECT * FROM employee ORDER BY salary DESC
answer     :
	id	name	contact_number	address	salary	employee_id	role	created_at	updated_at	
	1	nehav 	9833910534	mumbai	30000	98821	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	2	mina 	9833910535	thane	32000	98823	mangagement of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	3	pankaj 	9833910536	bhopal	40000	98824	valuing of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	4	mareena 	9833910537	meerut	45000	98825	revaluation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	5	pooja	9833910538	delhi	50000	98826	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	6	namita	9833910539	surat	52000	98820	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	7	sneha 	9833910510	baroda	55000	98827	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	8	anjali 	9833910511	ahmedabad	60000	98828	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	9	harsha 	9833910512	mumbai	20000	98829	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	10	varun 	9833910512	mehsana	56000	98831	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	11	preeti 	9833910513	noida	87000	98832	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	12	madhu 	9833910525	bangalore	22000	98833	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	



question 2 : select all employees from Mumbai 
sql query  : SELECT * FROM `employee` WHERE address = "mumbai"
answer     :
	id	name	contact_number	address	salary	employee_id	role	created_at	updated_at	
	1	nehav 	9833910534	mumbai	30000	98821	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	9	harsha 	9833910512	mumbai	20000	98829	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	



question 3 :  select all employees having salary more than average salary 
sql query  : SELECT * FROM `employee` WHERE salary > (SELECT AVG(salary) from employee)
answer     :
	id	name	contact_number	address	salary	employee_id	role	created_at	updated_at	
	5	pooja	9833910538	delhi	50000	98826	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	6	namita	9833910539	surat	52000	98820	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	7	sneha 	9833910510	baroda	55000	98827	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	8	anjali 	9833910511	ahmedabad	60000	98828	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	10	varun 	9833910512	mehsana	56000	98831	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	
	11	preeti 	9833910513	noida	87000	98832	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	



question 4 : select sum of salary from table 
sql query  : SELECT SUM(salary) AS "total salary" FROM employee WHERE salary
answer     :
	549000	



question 5 : select all unique address 
sql query  : SELECT DISTINCT address FROM employee
answer     :
	address	
	mumbai	
	thane	
	bhopal	
	meerut	
	delhi	
	surat	
	baroda	
	ahmedabad	
	mehsana	
	noida	
	bangalore	



question 6 : select details, salary from table
(details should be concatenation of name and address)
sql query  : SELECT CONCAT(name , address) AS "details" , salary FROM employee
answer     :
	details	salary	
	nehav mumbai	30000	
	mina thane	32000	
	pankaj bhopal	40000	
	mareena meerut	45000	
	poojadelhi	50000	
	namitasurat	52000	
	sneha baroda	55000	
	anjali ahmedabad	60000	
	harsha mumbai	20000	
	varun mehsana	56000	
	preeti noida	87000	
	madhu bangalore	22000	



question 7 : select names of employees having max salary
sql query  : SELECT Name, salary FROM employee WHERE salary=(SELECT MAX(salary) FROM employee)
answer     :
preeti 	87000	


		
question 8 : select employees having 2nd max salary 
sql query  : SELECT name, MAX(salary) AS salary FROM employee WHERE salary < (SELECT max(salary) FROM employee)
answer     :
	nehav 	60000	



question3 9 :  count emplyees by address, order by employee count
e.g. select count(name,address) from employee  	
---------------------------
city	|	emplyee count |
---------------------------
Mumbai	|		 4		  |
Nagpur	| 		 3		  |
Pune	| 		 2		  |
---------------------------	
sql query   : SELECT address ,count(address), COUNT(id) AS "Employee_Count" from employee
GROUP BY address
ORDER BY COUNT(id) DESC
answer      :
	address 	count(address)  	Employee_Count	
	mumbai	2	2	
	meerut	1	1	
	bangalore	1	1	
	ahmedabad	1	1	
	delhi	1	1	
	thane	1	1	
	mehsana	1	1	
	surat	1	1	
	bhopal	1	1	
	noida	1	1	
	baroda	1	1	



question 10 : count employees from mumbai only 
1sql query  : SELECT COUNT(address)FROM employee WHERE address = "mumbai"
answer      :
	2


	
question 12 : find employee having max salary in particular city
e.g. 
-------------------------------------
address 	|	name 	|	salary	|	
------------------------------------|
noida 		|	preeti 	|	87000	|
ahmedabad 	|	anjali 	|	60000	|
bangalore 	|	madhu 	|	60000	|
baroda 		|	varun 	|	56000	|
-----
sql query :SELECT *, MAX(salary) FROM employee GROUP BY address
answer    :
id	name	contact_number	address	salary	employee_id	role	created_at	updated_at	MAX(salary)	
8	anjali 	9833910511	ahmedabad	60000	98828	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	60000	
12	madhu 	9833910525	bangalore	22000	98833	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	22000	
7	sneha 	9833910510	baroda	55000	98827	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	55000	
3	pankaj 	9833910536	bhopal	40000	98824	valuing of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	40000	
5	pooja	9833910538	delhi	50000	98826	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	50000	
4	mareena 	9833910537	meerut	45000	98825	revaluation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	45000	
10	varun 	9833910512	mehsana	56000	98831	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	56000	
1	nehav 	9833910534	mumbai	30000	98821	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	30000	
11	preeti 	9833910513	noida	87000	98832	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	87000	
6	namita	9833910539	surat	52000	98820	reconciliation of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	52000	
2	mina 	9833910535	thane	32000	98823	mangagement of transactions	2021-05-27 00:00:00	2021-05-27 00:00:00	32000	




